# R Shiny Automodeler for Electron

This work uses the technique to export shiny apps as .exe files in the <https://github.com/COVAIL/electron-quick-start>.

The intentions of this project is to allow those with little to no R experience use a standalone app to create generalized linear models.

As of the current version, the only format the program takes is a csv file with 2 columns named 'x' and 'y', with y being the dependent variable and x being the independent variable. The program will check your assumptions, place it into a gaussian generalized linear model, give the R-squared and plot the data.
